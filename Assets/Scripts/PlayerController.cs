using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerController : MonoBehaviour
{
    // Private Variables
  private float speed = 20.0f; 
  private float turnSpeed = 25.0f;
  private float horizontalInput; 
  private float fowardInput;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // This is where I get my player Input 
        
        horizontalInput = Input.GetAxis("Horizontal");
      
       // Turn the Tank
       
       
        fowardInput = Input.GetAxis("Vertical");
       
        // Move the Tank foward for interact with obstacles
      
        transform.Translate(Vector3.forward * Time.deltaTime * speed * fowardInput);

        transform.Rotate(Vector3.up, Time.deltaTime* turnSpeed * horizontalInput);
    }

}
